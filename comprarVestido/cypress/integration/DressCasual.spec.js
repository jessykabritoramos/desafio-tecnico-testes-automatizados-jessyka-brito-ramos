import Logar from '../support/pages/Login/loginPage'


describe('Comprar vestido', () => {

    it('Fluxo Principal', () => {
        //Visitando a paginae validando a pagina
        Logar.open()
        //Logando o usuario, validando o item para compra
        Logar.form()
        //Adicionando produto no carrinho e validando dados de compra
        Logar.cart()
        //Forma de pagamento e confirmando pagamento
        Logar.paymentBank()
    });
    it('Fluxo Alternativo', () => {
        //Visitando a paginae validando a pagina
        Logar.open()
        //Logando o usuario, validando o item para compra
        Logar.form()
        //Adicionando produto no carrinho e validando dados de compra
        Logar.cart()
        //Forma de pagamento e confirmando pagamento
        Logar.paymentCheck()
    });

});
