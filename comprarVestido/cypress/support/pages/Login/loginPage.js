
class Logar {
    open() {
     //o usurio vai acessar o login da pagina
     cy.visit('http://automationpractice.com/index.php?id_category=9&controller=category')
     cy.get('#center_column').contains('Printed Dress')
     cy.get('.login').click()
     }
     //preenchendo os campos de email e senha para efeutar login na pagina, verificando item de compra
     form(email, password) {
         cy.get('#email').type('teste-desafio@gmail.com')  
         cy.get('#passwd').type('123456') 
         cy.get('#SubmitLogin > span').click() 
         cy.get('ul[class="submenu-container clearfix first-in-line-xs"]').invoke('css', 'display', 'block');
         cy.get(':nth-child(2) > .submenu-container > :nth-child(1) > a').click()
     }
     //Adicionando item no carrinho de compra 
     cart(){
        cy.get('.ajax_add_to_cart_button > span').click()
        cy.get('.button-container > .button-medium > span').click()
        //Validando o Sumary do carrinho
        cy.get('#center_column').contains('Printed Dress')
        cy.get('#center_column').contains('$29.12')
        //Validando adress
        cy.get('.cart_navigation > .button > span').click()
        cy.get('.cart_navigation > .button > span').click()
        //Validando shipping a forma de envio e a taxa de envio
        cy.get('#cgv').click()
        cy.get('td.delivery_option_price > .delivery_option_price').should('be.visible')
        cy.get('.cart_navigation > .button > span').click()
        //validando a forma de pagamento e concluindo a compra
        cy.get('.cart_description > .product-name').should('be.visible')
        cy.get('.label').should('be.visible')
        cy.get('#total_price').should('be.visible')
     }
     paymentBank(){
        //Escolhendo e validando a foma de pagamento *Pay by bank wire*
        cy.get('.bankwire').click()
        cy.get('.page-subheading').should('be.visible')
        //Confirmando o metodo de pagamento e concluindo compra
        cy.get('#cart_navigation > .button > span').click()
        cy.get('.box').should('be.visible')
        cy.get('.logout').click()
     }
     paymentCheck(){
        //Escolhendo a forma de pagamento *Pay by check*
        cy.get('.cheque').click()
        cy.get('.page-subheading').should('be.visible')
        //Confirmando o metodo de pagamento e concluindo a compra 
        cy.get('#cart_navigation > .button > span').click()
        cy.get('.page-subheading').should('be.visible') 
        cy.get('.logout').click()
     }
 }
 export default new Logar()