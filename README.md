# Desafio Técnico – Testes Automatizados – Jessyka Brito Ramos
Desafio técnido utilizando o framework cypress para automação da aplicação http://automationpractice.com/ o objetivo era criar um fluxo de testes para a compra de um determinado vestido.

## Description
Na automação utilizei de forma simples e bem objetiva a ferramenta de framework cypress com page object, extraí um relatório da execução através de um outro framework chamado allure, para quem se interessa, o cypress também fornece um relatório, porém a estetica não é legal, por isso fui pesquisar e encontrei o framework allure. 
Quer acessar o relatório né?
Tudo bem, vou te explicar...
Abre o porjeto ai na sua ide, em seguida abre o terminal e simbora digitar o seguinte comando "allure open", espera alguns instantes, pode demorar um minutinho, agora se com esse comando você ainda não conseguiu ter acesso ao relatório, sugiro que realize a instalação e configuração de ambiente. E olha que legal, vou te ajudar nessa!

## Installation

Como requisito é necessário que a sua máquina tenha o cypress instalado, pacote node.js, framework allure, scoop instalado e o java version 8.
Agora precisamos instalar o scoop, através do terminal windows powershel é só digitar os comandos:"Set-ExecutionPolicy RemoteSigned -Scope CurrentUser # Optional: Needed to run a remote script the first time". Pronto, agora podemos seguir para próxima etapa da configuração, prometo que falta pouco, logo logo você terá acesso ao projeto, o comando que vamos executar agora é o:"irm get.scoop.sh| iex" ainda no windows powershel.
Eu sei, ta ansioso né?
Calma, só mais um pouquinho de paciência e chegamos lá...
Agora vamos instalar o allure framework, com o seu projeto aberto na ide, abra o terminal que tem na sua ide, usei o vscode e digite o comando: "scoop install" para que o allure framework instale, após a instalação do framework, seguimos para instalação de seu plugin, para a instalção do plugin allure é necessário estar na pasta do projeto "cd tests/" e então partimos para executar o comandos: "npm i -D @shelex/cypress-allure-plugin". Pronto, agora sua ide está configurada e podemos executar o cypress.

## Usage
Com o terminal cypress aberto é só executar o seguinte comando: "npx cypress run --env allure=true"
Ai se você quiser executar o relatório é só digitar no terminal cypress "allure generate", caso queira abrir o relatório é só digitar no terminal "allure open", pode demorar alguns instantes para abrir o relatório, mas vale a pena.

## Authors and acknowledgment
Jéssyka Brito Ramos
